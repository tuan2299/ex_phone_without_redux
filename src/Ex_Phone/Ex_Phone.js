import React, { Component } from "react";
import { data_Phone } from "./data_Phone";
import DetailPhone from "./DetailPhone";
import ListPhone from "./ListPhone";

class Ex_Phone extends Component {
  state = {
    listPhone: data_Phone,
    detail: data_Phone[0],
  };
  render() {
    return (
      <div>
        <h2>Ex_Phone</h2>
        <ListPhone list={this.state.listPhone} />
        <DetailPhone detail={this.state.detail} />
      </div>
    );
  }
}

export default Ex_Phone;
